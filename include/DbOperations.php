<?php
	
	class DbOperations	{
		private $con;

		function __construct()	{
			require_once dirname(__FILE__).'/DbConnect.php';
			$db=new DbConnect();
			$this->con=$db->connect();			
		}
		private function doesUserExist($email)	{
			$stmt=$this->con->prepare("select user_id from users where email=?");
			$stmt->bind_param("s", $email);
			$stmt->execute();
			$stmt->store_result();
			return $stmt->num_rows==0;
		}
		
		public function getUserId($email)	{
			$stmt=$this->con->prepare("select user_id from users where email=?");
			$stmt->bind_param("s",$email);
			$stmt->execute();			
			$stmt->bind_result($user_id);
			$stmt->fetch();
			return $user_id;
		}
		public function registerUser($email, $pass, $name, $mobile_no, $role, $photo_file, $photo_url, $device_token)	{
			if($this->doesUserExist($email))	{
				$password=md5($pass);				
				$stmt=$this->con->prepare("insert into users(user_id, email, password, name, mobile_no, role, photo_url, device_token) value(null, ?, ?, ?, ?, ?, ?, ?)");
				$stmt->bind_param('sssisss', $email, $password, $name, $mobile_no, $role, $photo_url, $device_token);
				if($stmt->execute())	{
					$image_file_decoded=base64_decode($photo_file);
					file_put_contents('../uploads/profiles/'.$photo_url, $image_file_decoded);
					$user_id=$this->getUserId($email);
					return array("result"=>"1", "user_id"=>strval($user_id));
				} else	{
					return array("result"=>"0");
				}
			} else	{
				return array("result"=>"2");
			}
		}	
		public function loginUser($email, $pass, $device_token)	{
			$password=md5($pass);
			$stmt=$this->con->prepare("select user_id from users where email=? and password=?");
			$stmt->bind_param("ss", $email, $password);	
			$stmt->execute();
			$stmt->store_result();
			if($stmt->num_rows>0)	{
				$stmt=$this->con->prepare("update users set device_token=? where email=?");
				$stmt->bind_param("ss", $device_token, $email);
				if($stmt->execute())	{	
					$user_id=$this->getUserId($email);
					return array("result"=>"1", "user_id"=>strval($user_id));
				}
				
			} else	{
				return array("result"=>"2");
			}
		}	
		public function getMemberDetails($user_id)	{
			$stmt=$this->con->prepare("select email, name, mobile_no, role, photo_url from users where user_id=?");
			$stmt->bind_param('i', $user_id);
			if($stmt->execute())	{
				$result=$stmt->get_result()->fetch_assoc();
				$result["result"]="1";
				return $result;
			} else	{
				return array("result"=>"0");
			}				
		}	
		public function getProfileImage($user_id)	{
			$stmt=$this->con->prepare("select photo_url from users where user_id=?");
			$stmt->bind_param("i", $user_id);
			if($stmt->execute())	{
				$result=$stmt->get_result()->fetch_assoc();
				$result["result"]="1";
				return $result;
			}	else	{
				return array("result"=>"0");	
			}	
		}	
		public function setProfileImage($user_id, $photo_file, $photo_url)	{
			$stmt=$this->con->prepare("update users set photo_url=? where user_id=?");
			$stmt->bind_param("si", $photo_url, $user_id);
			if($stmt->execute())	{
				$image_file_decoded=base64_decode($photo_file);
				file_put_contents('../uploads/profiles/'.$photo_url, $image_file_decoded);
				return array("result"=>"1", "photo_url"=>$photo_url);
			} else	{
				return array("result"=>"0");
			}	
		}	
		public function updateUserDetails($user_id, $email, $name, $mobile_no, $role)	{
			$stmt=$this->con->prepare("update users set email=?, name=?, role=?, mobile_no=? where user_id=?");
			$stmt->bind_param("sssii", $email, $name, $mobile_no, $role, $user_id);
			if($stmt->execute())	{
				return array("result"=>"1");
			} else	{
				return array("result"=>"0");
			}	
		}	
		public function getAllMembers($start_index)	{
			$stmt=$this->con->prepare("select user_id, name, photo_url from users order by name asc limit ?, 10");
			$stmt->bind_param("i",$start_index);
			if($stmt->execute())	{
				$result=$stmt->get_result();
				$rowsArray=array();
				while($row=$result->fetch_assoc())	{
					$rowsArray[]=$row;	
				}	
				if(count($rowsArray)>0)	{
					return array("result"=>"1", "members"=>$rowsArray);
				} else	{
					return array("result"=>"2");
				}					
			} else	{
				return array("result"=>"0");
			}	
		}
		public function addNewPost($title, $description, $photo_file, $pic_url, $timestamp, $user_id)	{
			$stmt=$this->con->prepare("insert into posts(post_id, title, description, pic_url, timestamp, user_id) value(null, ?, ?, ?, ?, ?)");
			$stmt->bind_param("sssss", $title, $description, $pic_url, $timestamp, $user_id);
			if($stmt->execute())	{
				$image_file_decoded=base64_decode($photo_file);
				file_put_contents('../uploads/posts/'.$pic_url, $image_file_decoded);
				return array("result"=>"1");
			} else	{
				return array("result"=>"0");
			}	
		}		
		public function getAllPosts($currentUser, $start_index)	{
			$stmt=$this->con->prepare("select users.user_id, users.name, users.photo_url, posts.post_id, posts.title, posts.description, posts.pic_url 
											from posts inner join users on posts.user_id=users.user_id order by timestamp desc limit ?, 4");		
			$stmt->bind_param("i", $start_index);		
			if($stmt->execute())	{
				$rowsArray=array();
				$postResult=$stmt->get_result();
				while($row=$postResult->fetch_assoc())	{	
					$statusStmt=$this->con->prepare("select status from attendance where post_id=? and user_id=?");	
					$statusStmt->bind_param("ii",$row["post_id"], $currentUser);
					if($statusStmt->execute())	{
						$status=$statusStmt->get_result()->fetch_assoc()["status"];
						if(strlen($status)>0)	{
							$row["status"]=$status;
						}	else	{
							$row["status"]="";
						}
						array_push($rowsArray, $row);						
					}					
				}		
				if(count($rowsArray)>0)	{
					return array("result"=>"1", "posts"=>$rowsArray);
				}	else	{
					return array("result"=>"2");
				}	
			}	else	{
				return array("result"=>"0");
			}	
		}	
		public function doesStatusExist($postId, $currentUser)	{
			$stmt=$this->con->prepare("select status from attendance where post_id=? and user_id=?");			
			$stmt->bind_param("ii", $postId, $currentUser);
			
			if($stmt->execute())	{				
				$stmt->store_result();		
				return $stmt->num_rows;
			}	
		}	
		
		public function updateAttendanceStatus($postId, $currentUser, $status)	{			
			if($status=='')	{
				$stmt=$this->con->prepare("delete from attendance where post_id=? and user_id=?");
				$stmt->bind_param("ii", $postId, $currentUser);
				if($stmt->execute())	{
					return array("result"=>"1");
				}	else	{
					return array("result"=>"0");
				}	
			}	
			else	{
				$sqlQuery="";
				if($this->doesStatusExist($postId, $currentUser))	{
					$sqlQuery="update attendance set status=? where post_id=? and user_id=?";
				}	else	{
					$sqlQuery="insert into attendance(attendance_id, status, post_id, user_id) value(null, ?, ?, ?)";
				}	
				$stmt=$this->con->prepare($sqlQuery);
				$stmt->bind_param("sii", $status, $postId, $currentUser);
				if($stmt->execute())	{
					return array("result"=>"1");
				}	else	{
					return array("result"=>"0");
				}	
			}
		}	
		
		public function getPostAttendance($postId, $status)	{
			$stmt=$this->con->prepare("select users.user_id, users.name, users.photo_url, attendance.status from attendance inner join users on attendance.user_id=users.user_id and post_id=? and status=?");
			$stmt->bind_param("is",$postId, $status);
			if($stmt->execute())	{
				$rowsArray=array();
				$result=$stmt->get_result();
				while($row=$result->fetch_assoc())	{
					$rowsArray[]=$row;
				}	
				if(count($rowsArray)>0)	{
					return array("result"=>"1", "attendance"=>$rowsArray);
				}	else	{
					return array("result"=>"2");
				}	
			}	else	{
				return array("result"=>"0");
			}	
		}
		public function getSinglePost($postId, $currentUser)	{
			$stmt=$this->con->prepare("select users.user_id, users.name, users.photo_url, posts.title, posts.description, posts.pic_url from posts inner join users on posts.user_id=users.user_id and posts.post_id=?");
			$stmt->bind_param("i", $postId);
			if($stmt->execute())	{
				$result=$stmt->get_result()->fetch_assoc();
				$statusStmt=$this->con->prepare("select status from attendance where post_id=? and user_id=?");	
				$statusStmt->bind_param("ii",$postId, $currentUser);
				if($statusStmt->execute())	{
					$status=$statusStmt->get_result()->fetch_assoc()["status"];
					$nameStmt=$this->con->prepare("select name from users where user_id=?");
					$nameStmt->bind_param("i", $currentUser);
					$nameStmt->execute();
					$currentUserName=$nameStmt->get_result()->fetch_assoc()["name"];
					$result["current_user_name"]=$currentUserName;
					if(strlen($status)>0)	{
						$result["status"]=$status;
					} else	{
						$result["status"]="";
					}
					$result["result"]="1";
					return $result;
				} else	{
					return array("result"=>"0");
				}						
			} else	{
				return array("result"=>"0");
			}
		}	
		public function deletePost($postId)	{
			$tableNamesArray=array("attendance", "comments", "posts");
			foreach($tableNamesArray as $tableName)	{
				$stmt=$this->con->prepare("delete from ".$tableName." where post_id=?");
				$stmt->bind_param("i", $postId);
				$stmt->execute();
			}				
			return array("result"=>"1");
		}	
		
		public function getAllComments($postId)	{
			$stmt=$this->con->prepare("select comment_id, message, comments.user_id, users.name from comments inner join users on comments.user_id=users.user_id and post_id=?");
			$stmt->bind_param("i", $postId);
			if($stmt->execute())	{
				$rowsArray=array();
				$result=$stmt->get_result();
				while($row=$result->fetch_assoc())	{
					$rowsArray[]=$row;
				}					
				return array("result"=>"1", "comments"=>$rowsArray);
			} else	{
				return array("result"=>"0");
			}	
		}
		
		public function addComment($message, $postId, $currentUser)	{
			$stmt=$this->con->prepare("insert into comments(comment_id, message, post_id, user_id) value(null, ?, ?, ?)");
			$stmt->bind_param("sss", $message, $postId, $currentUser);
			if($stmt->execute())	{
				return array("result"=>"1");
			}	else	{
				return array("result"=>"0");
			}	
		}	
	}	
?>