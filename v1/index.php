<?php
	require_once '../include/DbOperations.php';
	
	$response=array();
	
	if($_SERVER["REQUEST_METHOD"]=="POST")	{				
		if(isset($_POST["action"]))	{			
			$db=new DbOperations();
			switch(trim($_POST["action"]))	{
				case "registerUser":
					$response=$db->registerUser($_POST["email"], $_POST["password"], $_POST["name"], $_POST["mobile_no"], $_POST['role'], $_POST['photo_file'], $_POST["photo_url"], $_POST["device_token"]);
					break;
				case "loginUser":
					$response=$db->loginUser($_POST["email"], $_POST["password"], $_POST["device_token"]);					
					break;
				case "getMemberDetails":
					$response=$db->getMemberDetails($_POST["user_id"]);
					break;
				case "getProfileImage":
					$response=$db->getProfileImage($_POST["user_id"]);
					break;
				case "setProfileImage":
					$response=$db->setProfileImage($_POST["user_id"], $_POST["photo_file"], $_POST["photo_url"]);
					break;	
				case "updateUserDetails":
					$response=$db->updateUserDetails($_POST["user_id"], $_POST["email"], $_POST["name"], $_POST['role'], $_POST["mobile_no"]);
					break;
				case "getAllMembers":
					$response=$db->getAllMembers($_POST["start_index"]);
					break;
				case "addNewPost":
					$response=$db->addNewPost($_POST["title"], $_POST["description"], $_POST["photo_file"], $_POST["pic_url"], $_POST["timestamp"], $_POST["user_id"]);
					break;	
				case "getAllPosts":
					$response=$db->getAllPosts($_POST["current_user"], $_POST["start_index"]);
					break;	
				case "updateAttendanceStatus":
					$response=$db->updateAttendanceStatus($_POST["post_id"], $_POST["current_user"], $_POST["status"]);
					break;
				case "getPostAttendance":
					$response=$db->getPostAttendance($_POST["post_id"], $_POST["status"]);
					break;
				case "getSinglePost":
					$response=$db->getSinglePost($_POST["post_id"], $_POST["current_user"]);
					break;	
				case "deletePost":
					$response=$db->deletePost($_POST["post_id"]);
					break;	
				case "getAllComments":
					$response=$db->getAllComments($_POST["post_id"]);
					break;			
				case "addComment":
					$response=$db->addComment($_POST["message"], $_POST["post_id"], $_POST["current_user"]);
					break;
				case "sendNotification":
					$response=$db->sendNotification($_POST["title"], $_POST["message"], $_POST["device_token"]);
					break;	
			}	
		}	
	}
	echo json_encode($response);
?>